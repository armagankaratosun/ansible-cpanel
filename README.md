# Ansible Role: Install cPanel & WHM


## Description

This role bootstraps your cPanel & WHM on remote fresh CentOS hosts. Devel branch
aims to improve this role with some tweaks and new features. See [TODO](TODO) section bellow
for more info. For more stable version, check main branch of this role.

## Dependencies

- Ansible 2.4
- CentOS 6.5+
- Knowledge of SSH and authenticating

## TODO

- [x] cPanel Pre-Configurations
- [x] EA4 Configurations
- [x] Housekeeping
- [ ] Mail Queue Configurations
- [ ] SNMP Installation
- [ ] Patchman Install & Configurations
- [ ] Softoculus Intsall & Configurations
- [ ] CSF Install & Configurations


## License
GPLv2 license; full text can be find in LICENSE file.
